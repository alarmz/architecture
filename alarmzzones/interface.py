from abc import ABC, abstractmethod
from alarmdevices.interface import SensorId
from enum import Enum

class ZoneAlarmState(Enum):

    NORMAL = 0
    VERIFY = 1
    PENDING = 2
    SILENT_ALARM = 3
    ALARM = 4
    RESTORED = 4
    SWINGER_SHUTDOWN_REACHED = 5


class ZoneState(Enum):

    DISARMED = 0
    ARMED_STAY = 1
    ARMED_AWAY = 2
    SOUNDING = 3
    RE_ARMED_STAY = 4
    RE_ARMED_AWAY = 5


class ZoneTroubleType(Enum):

    NONE = 0
    LOW_BATTERY = 1
    TAMPERED = 2
    OTHER = 100


class ZoneTroubleEvent(object):

    def __init__(self, zone_id, sensor_id: SensorId, trouble_type: ZoneTroubleType, trouble_state: bool):
        self.trouble_type = trouble_type
        self.trouble_state = trouble_value


class ZoneAlarmEvent(object):

    def __init__(self, zone_id, sensor_id: SensorId, zone_alarm_state: ZoneAlarmState):
        self._zone_id = zone_id
        self._sensor_id = sensor_id
        self._zone_state = zone_state


class AbstractZone(ABC):

    def __init__(self, zone_id, device_obj, sensor_id):
        self._id = zone_id
        self._device = device_obj
        self._sensor_id = sensor_id
        self._alarmProcessQueue = asyncio.Queue()
        self._troubleProcessQueue = asyncio.Queue()
        self._process_device_events_task = asyncio.create_task(self._process_device_events())

    async def _process_device_events(self):
        async for device_event in self._device.async_event_generator(self._sensor_id):
            alarm_copy = copy(device_event)
            trouble_copy = copy(device_event)
            await asyncio.gather(self._alarmProcessQueue.put(alarm_copy)
                                 self._troubleProcessQueue.put(trouble_copy)

    @abstractmethod
    def set_zone_alarm_state(self, new_state: ZoneAlarmState):
        """Sets the zone alarm state for this zone.
        """
        pass

    @abstractmethod
    async def async_event_generator(self):
        """Async Generator for ZoneAlarmEvents
        """
        pass

    @abstractmethod
    async def async_trouble_generator(self):
        """Async Generator for ZoneTroubleEvents.
        """

    
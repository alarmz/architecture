import interface
import asyncio
from copy import copy

class PerimeterZone(interface.AbstractZone):

    def __init__(self, zone_id, device_obj, sensor_id, zone_state: interface.ZoneState):
        super().__init__(zone_id, device_obj, sensor_id)
        self._zone_state = zone_state
        self._has_low_battery = False
        self._sensor_tripped = False

    def set_zone_alarm_state(self, new_state: interface.ZoneAlarmState):
        self._zone_state = new_state


    async def async_event_generator(self):
        while True:
            device_event = await self._alarmProcessQueue.get()
            if device_event.value:
                self._sensor_tripped = True
                if self._zone_state != interface.ZoneState.DISARMED:
                    pending_timer = asyncio.create_task(lambda: asyncio.sleep(60))
                    yield interface.ZoneAlarmEvent(self._id, self._sensor_id, interface.ZoneAlarmState.PENDING)
                    await pending_timer
                    yield interface.ZoneAlarmEvent(self._id, self._sensor_id, interface.ZoneAlarmState.ALARM)
            else:
                self._sensor_tripped = False

    async def async_trouble_generator(self):
        while True:
            device_event = await self._troubleProcessQueue.get()
            if device_event.low_battery:
                self._has_low_battery = True
                yield interface.ZoneTroubleEvent(self._id, self._sensor_id, interface.ZoneTroubleType.LOW_BATTERY, True)
            elif self._has_low_battery:
                self._has_low_battery = False
                yield interface.ZoneTroubleEvent(self._id, self._sensor_id, interface.ZoneTroubleType.LOW_BATTERY, True)
                


from abc import ABC, abstractmethod
from collections import namedtuple

SensorId = namedtuple('SensorId', ['device_id', 'sensor_id'])

class DeviceEvent(object):

    def __init__(self, device_id, sensor_id, value, low_battery=False):
        self._value = value
        self._low_battery = low_battery
        self._sensor = SensorId(device_id, sensor_id)

    @property
    def sensor(self):
        return self._sensor

    @property
    def value(self):
        return self._value

    @property
    def low_battery(self):
        return self._low_battery
    

class AbstractDevice(ABC):

    @abstractmethod
    async def async_event_generator(self, sensor_id):
        """Generator for DeviceEvents for the provided sensor.
        """
        pass

    @property
    @abstractmethod
    def device_id(self):
        """Return the device ID
        """
        pass

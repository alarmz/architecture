# Alarmz Architecture

```plantuml

package System {
    [State]
    [Troubles] --> [Sentry AI]
    [Alarms] --> [State]
    [Control] --> [State]
    [Sentry AI] --> [State]
    [History] --> [State]
}

package Zone {
    () "Sensing Zone"
    () "Controlling Zone"

    [Sensing Zone] -- [Binary Sensor Zone]
    [Sensing Zone] -- [Threshold Zone]
    [Sensing Zone] -- [AI Sensing Zone]

    [Controlling Zone] -- [Control Zone]
    [Controlling Zone] -- [AI Control Zone]
}

[State] ==> [Sensing Zone]
[Control] ==> [Controlling Zone]
[Sentry AI] ==> [Sensing Zone]

[Troubles] ..> [Controlling Zone]
[Troubles] ..> [Sensing Zone]

package "Input Device" {
    [Binary Device]
    [Multilevel Device]
    [Control Device]

    package Analytics {
        [Image Analytics]
        [Audio Analytics]
    }

    package Sensor {
        [Binary Sensor]
        [Multilevel Sensor]
        [Camera]
        [Microphone]
    }
}


package "Output Device" {
    [Siren]
    [Strobe]
    [Annunciator] ==> [Speaker]
}

[Binary Sensor Zone] --> [Binary Device]
[AI Sensing Zone] ==> [Binary Device]
[AI Sensing Zone] ==> [Multilevel Device]
[Threshold Zone] --> [Multilevel Device]
[Threshold Zone] ..> [Image Analytics]
[Threshold Zone] ..> [Audio Analytics]
[AI Sensing Zone] ..> [Image Analytics]
[AI Sensing Zone] ..> [Audio Analytics]
[AI Control Zone] ..> [Image Analytics]
[AI Control Zone] ..> [Audio Analytics]
[Control Zone] --> [Control Device]

[State] ..> [Siren]
[State] ..> [Strobe]
[State] ..> [Annunciator]



[Binary Device] ==> [Binary Sensor]
[Multilevel Device] ==> [Multilevel Sensor]

[Image Analytics] --> [Camera]
[Audio Analytics] --> [Microphone]

```

### Definitions

A **binary sensor** simply provides an on/off signal.

A **multilevel sensor** provides a numeric signal.

A **device** is a collection of sensors, including battery level sensors and tamper switch sensors.  If a device includes a _multilevel sensor_ it defines the minimum and maximum values that sensor can provide.

A **control device** is something like a keyfob or keypad which is 
